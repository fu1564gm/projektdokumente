# Projekt Dokumente

Wichtig: Es gibt keine falschen Fragen/Ideen je mehr alle Mitarbeiten desto besser wird das ganze Projekt am Ende. Wenn jemand was nicht kann, einfach fragen und falls niemand helfen kann wissen wir wo wir alle noch nachbessern müssen

## Installation

Denkt dran bevor ihr das GIT clonen und commits tätigen könnt, müsst ihr erst euren [SSH Schlüssel hinzufügen](https://docs.gitlab.com/ee/user/ssh.html)

### Docker + WSL in Windows

Vorteil: Ermöglicht viele spezialisierte Entwicklungsumgebungen getrennt voneinander und vieles mehr

- Aktiviert [WSL2](https://learn.microsoft.com/en-us/windows/wsl/install) in Windows 

- Downloade & Installiere [Docker Desktop](https://docs.docker.com/desktop/install/windows-install/)

- [ ] Entweder ein [Container anpassen](https://medium.com/bb-tutorials-and-thoughts/vue-js-local-development-with-docker-compose-275304534f7c)

- [ ] Oder LinuxDistro:latest Container ummodellieren, Nachteil identisch zu WSL2 ohne Docker

### WSL2 in Windows

Vorteil: "Linux Maschine" in Windows, erleichtert Packet und Versionsverwaltung

- Aktiviert [WSL2](https://learn.microsoft.com/en-us/windows/wsl/install) in Windows 

- Linux Distro aus dem Microsoft Store installieren

- Distro konfigurieren

- (optional) Installiere NVM 
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
```
- (optinal) wähle die aktuelle node lts Version
```
nvm install --lts
```

### Linux und MacOS

- Googelt

## TO-DOs


### Design berwerten + anpassen

- [ ] [Schriftart, -größe, -platzierung](https://www.mehr-demokratie.de/)
- [ ] [Farben](https://www.mehr-demokratie.de/)
- [ ] [Whitespaces](https://webflow.com/blog/web-design-principles)
- [ ] [Visual Flow/ Roter Faden](https://webflow.com/blog/how-to-design-a-website)

Input:

- [The web design process: creating the visual design](https://webflow.com/blog/the-web-design-process-creating-the-visual-design)
- [10 principles of good web design](https://webflow.com/blog/web-design-principles)
- [Achieve Accessibility](https://web.dev/accessible/)

### Zielplatformen

- [X] Desktop
- [ ] Handy
- [ ] Tablet

### Nutzer

- [ ] Nutzergruppen und -typen identifizieren
- [ ] [Userstorys verfassen](https://www.atlassian.com/agile/project-management/user-stories)

## Gitlab konfigurieren

### Integrate with your tools

- [ ] [Set up project integrations](https://git.imp.fu-berlin.de/fu1564gm/projektdokumente/-/settings/integrations)

### Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

### Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)